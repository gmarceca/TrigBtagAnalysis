This code compiles and runs within the compiled ATLAS libraries in lxplus machines at CERN.

Steering code:
testTrigBtagAnalysis.py

Main codes:
TrigBtagTuning.cxx
TrigBtagTuning.h



Setup:

setupATLAS

mkdir source build run 
cd source
asetup devval,rel_1,here
svnco -t Trigger/TrigAnalysis/TrigBtagAnalysis

Compilation:

cd ../build
cmake ../source
make -j4

cd ../run
source ../build/x86_64-slc6-gcc49-opt/setup.sh
cp ../source/Trigger/TrigAnalysis/TrigBtagAnalysis/share/testTrigBtagAnalysis.py .

Running:

athena testTrigBtagAnalysis.py

Setup your configurable:

in testTrigBtagAnalysis.py you can setup the number of events to run, the input, the output, the Tuning/Tagging mode, the chain, etc...

Number of events where your job will run: (Line 16). (use -1 to run in all events of the input)

Input file: (Line 29). You can use regex to specify your input.

Tuning/Tagging mode and chain selected: (Line 86). DoTuning=True to run in tuning mode, DoTagging=True to run in tagging mode.

Output file: (Line 101).

Chain: the name of the chain should be specified in line 86 of the testTrigBtagAnalysis.py. Also the way of accessing the container information needs to be changed for split and non split chains. Take a look at lines 345/347, 359/361 and 373/375 in source/Trigger/TrigAnalysis/TrigBtagAnalysis/src/TrigBtagTuning.cxx

Short explanation of the code:
This code runs in two sequential modes: First the tuning mode and second the tagging mode


Tuning mode (Training):

This is what you should do first if you want to get your own PDFs weights.

After running in the tuning mode you should rename the output to tuning.root.
The tuning.root file will contain the PDFs necessary to get your own IPXD, SV1 and COMB weights.

To run in the tuning mode, set DoTuning=True in line 86 of testTrigBtagAnalysis.py.


Tagging mode (Performance Validation):

This mode gets your PDFs distributions after running in the tuning mode and calculates your own IPXD, SV1 and COMB weights.

To run in the tagging mode, set DoTagging=True in line 86 of testTrigBtagAnalysis.py.
