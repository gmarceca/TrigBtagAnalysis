/////////////////////////// -*- C++ -*- /////////////////////////////
// TrigBtagTuning.h 
// Header file for class TrigBtagTuning
// Authors:
//         Carlo Schiavi <carlo.schiavi@cern.ch>
//         Florencia Daneri <maria.florencia.daneri@cern.ch>
//         Gino Marceca <gino.marceca@cern.ch>
/////////////////////////////////////////////////////////////////// 
#ifndef TRIGBTAGANALYSIS_TUNING_H
#define TRIGBTAGANALYSIS_TUNING_H 1

// STL includes
#include <string>

// FrameWork includes
#include "AthenaBaseComps/AthHistogramAlgorithm.h" ////FIXME AthHistogramAlgorithm -> AthAlgorithm 
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigConfInterfaces/ITrigConfigSvc.h"
#include "StoreGate/StoreGate.h"
#include "StoreGate/StoreGateSvc.h"

//#include "JetTagTools/TrackSelector.h"

#include <TH1.h>
#include <TH2.h>
#include <TMath.h>
#include <TFile.h>
#include <TLorentzVector.h>
#include <TMath.h>

#include <iostream>
#include <ctime>

//namespace Trk  { class ITrackToVertexIPEstimator; }

class TrigBtagTuning
  : public ::AthHistogramAlgorithm  //FIXME AthHistogramAlgorithm -> AthAlgorithm 
{ 

public: 

  /// Constructor with parameters: 
  TrigBtagTuning( const std::string& name, ISvcLocator* pSvcLocator );

  /// Destructor: 
  virtual ~TrigBtagTuning(); 

  // INPUT OUTPUT FILES AND HISTOGRAMS
//  TFile *m_outfile;
  TFile *m_infile;

  double DRmin(TLorentzVector myjet, std::vector<TLorentzVector> jets, double PtMin); 
  int TruthMatch(TLorentzVector myjet, std::vector<TLorentzVector> jets, std::vector<TLorentzVector> &matchingJets, double Cut);

  float d0Sign(float d0, float phi, float jetPhi);
  float z0Sign(float z0, float eta, float jetEta);
  float Calculate_Mean (TLorentzVector myjet, const xAOD::TrackParticleContainer* tpContainers);


  // MAIN METHODS
  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

  // PROPERTIES
  std::string m_bjetItem;
  int   m_TrkSel_BLayer;
  int   m_TrkSel_InnerLayer;
  int   m_TrkSel_NextLayer;
  int   m_TrkSel_PixHits;
  int   m_TrkSel_SiHits;
  float m_TrkSel_D0;
  float m_TrkSel_Z0;
  float m_TrkSel_Pt;
  float  m_fitChi2;
  float m_fitProb;
  float m_fitChi2OnNdfMax;

  bool  m_doTuning;
  bool  m_doTagging;

  double Pl_havingSV;
  double Pb_havingSV;

  // SERVICES
  ToolHandle<Trig::TrigDecisionTool> m_trigDec; //!
  ServiceHandle<StoreGateSvc> m_storeGate; //!
  ServiceHandle< TrigConf::ITrigConfigSvc > m_configSvc; 
  ServiceHandle< TrigConf::ITrigConfigSvc > m_dsSvc;

//ToolHandle< Trk::ITrackToVertexIPEstimator > m_trackToVertexIPEstimator; //!

  //TrackSelector m_trackSelectorTool;


  /////////////////////////////////////////////////////////////////// 
  // ANALYSIS METHODS
  ///////////////////////////////////////////////////////////////////
  StatusCode executeBjetTuning(std::string trigItem);
  void bjetLabel(std::vector<const xAOD::Jet*> jetContainers, const xAOD::JetContainer* truthjets, double maxDR, double minPt, std::vector<int>& onlinejets_label,std::vector<int>& truth_indices);
 
  void bjetIsolation(std::vector<const xAOD::Jet*> jetContainers, double maxDR, std::vector<bool>& onlinejets_isolated);

  void bjetMatchOffline(std::vector<const xAOD::Jet*> jetContainers, double minDR, std::vector<bool>& onlinejets_offline_matching, std::vector<int>& offl_indices);
  
  void bjetTracks(std::vector<const xAOD::Jet*> jetContainers, std::vector<const xAOD::TrackParticleContainer*> tpContainers, double maxDR, std::vector<int>& onlinejets_trks);

  bool selectTrack(const xAOD::TrackParticle* onlinetrk, float zBS, float zv, bool &type);

  void tuneJet(const xAOD::Jet* jetContainers, const xAOD::BTagging* btagContainers, const xAOD::TrackParticleContainer* onlinetrks, const xAOD::Vertex* pvContainers, int label);
  
  
  void tagJet(const xAOD::Jet* jetContainers, const xAOD::BTagging* btagContainers, const xAOD::Jet* offlinejet, const xAOD::TrackParticleContainer* onlinetrks, const xAOD::Vertex* pvContainers, int label, bool nontracks);
  

  void control_cuts (int i);
  void control_Trkcuts (int i);
  std::string m_trigDecisionKey;


private: 

int track_index;

  /// Default constructor: 
  TrigBtagTuning();

  // Histograms
//  TH1F* b_output_IP1D;
//  TH1F* b_output_IP2D;
//  TH2F* b_output_IP3D;
//  TH1F* b_output_IP1D_loose;
//  TH1F* b_output_IP2D_loose;
//  TH2F* b_output_IP3D_loose;
//  TH1F* b_output_MVTX;
//  TH1F* b_output_EVTX;
//  TH1F* b_output_NVTX;
//  TH1F* l_output_IP1D;
//  TH1F* l_output_IP2D;
//  TH2F* l_output_IP3D;
//  TH1F* l_output_IP1D_loose;
//  TH1F* l_output_IP2D_loose;
//  TH2F* l_output_IP3D_loose;
//  TH1F* l_output_MVTX;
//  TH1F* l_output_EVTX;
//  TH1F* l_output_NVTX;
//
//
//TH1F* h_minDR_onl_off;
//
//
//TH1F* h_errorz0;
//TH1F* h_errord0;
//
//TH1F* h_offl_index;
//TH1F* h_JetLabel;
//TH1F* h_pt_trk;
//TH1F* h_pt_onl;
//TH1F* h_pt_offl;
//TH1F* h_eta_onl;
//TH1F* h_pt_truth;
//TH1F* h_minDR;
//TH1F* h_DR_trk_jet;
//TH1F* h_index_trk_jet;
//
//TH1F* h_zv_offline;
//TH1F* h_zv_online;
//TH1F* h_diffzv;
//TH1F* h_zBS_online;
//
//TH1F* h_counter;
//TH1F* h_controlCuts;
//TH1F* h_controlTrkCuts;
//
//
//  
  TH1F* b_input_IP1D; 
  TH1F* b_input_IP2D;
  TH2F* b_input_IP3D;
  
  TH1F* b_input_IP1D_loose; 
  TH1F* b_input_IP2D_loose;
  TH2F* b_input_IP3D_loose;
  
  TH1F* b_input_MVTX;
  TH1F* b_input_EVTX;
  TH1F* b_input_NVTX;

  TH1F* b_input_MVTX_Bin0;
  TH1F* b_input_EVTX_Bin0;
  TH1F* b_input_NVTX_Bin0;

  TH1F* l_input_IP1D;
  TH1F* l_input_IP2D;
  TH2F* l_input_IP3D;
  
  TH1F* l_input_IP1D_loose;
  TH1F* l_input_IP2D_loose;
  TH2F* l_input_IP3D_loose;

  TH1F* l_input_MVTX;
  TH1F* l_input_EVTX;
  TH1F* l_input_NVTX;

  TH1F* l_input_MVTX_Bin0;
  TH1F* l_input_EVTX_Bin0;
  TH1F* l_input_NVTX_Bin0;
//
//  TH1F* b_tag_IP1D;
//  TH1F* b_tag_IP2D;
//  TH1F* b_tag_IP3D;
//  TH1F* b_tag_COMB;
//  TH1F* b_tag_SV1;
//  TH1F* l_tag_IP1D;
//  TH1F* l_tag_IP2D;
//  TH1F* l_tag_IP3D;
//  TH1F* l_tag_COMB;
//  TH1F* l_tag_SV1;
//
//// 1-D offline
TH1F *hb_offIP3D;
TH1F *hl_offIP3D;


//TH1F* h_deltaErrZ0;
//TH1F* h_deltaErrD0;


}; 


#endif //> !TRIGBTAGANALYSIS_TRIGBTAGTUNING_H
