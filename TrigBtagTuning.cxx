///////////////////////// -*- C++ -*- /////////////////////////////
// TrigBtagTuning.cxx 

// Implementation file for class TrigBtagTuning

// Authors:
//         Carlo Schiavi <carlo.schiavi@cern.ch>
//         Florencia Daneri <maria.florencia.daneri@cern.ch>
//         Gino Marceca <gino.marceca@cern.ch>

// On behalf the ATLAS Collaboration
/////////////////////////////////////////////////////////////////// 

#include "TrigBtagAnalysis/TrigBtagTuning.h"
#include "Particle/TrackParticleContainer.h"
#include "TrigInDetEvent/TrigVertexCollection.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAttributes.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h" 
#include "xAODTracking/TrackParticleAuxContainer.h" 
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODBTagging/SecVtxHelper.h"

#include "CLHEP/GenericFunctions/CumulativeChiSquare.hh"

#include "GaudiKernel/Property.h"
#include "EventPrimitives/EventPrimitives.h"
#include "EventPrimitives/EventPrimitivesHelpers.h"

#include <cmath>
#include <algorithm>
#include <iostream>
#include <iomanip>


TrigBtagTuning::TrigBtagTuning( const std::string& name, 
        ISvcLocator* pSvcLocator ) : 
    ::AthHistogramAlgorithm ( name, pSvcLocator ), //FIXME AthHistogramAlgorithm -> AthAlgorithm 
    m_trigDec("Trig::TrigDecisionTool/TrigDecisionTool"),
    m_storeGate("StoreGateSvc", name),
    m_configSvc( "TrigConf::TrigConfigSvc/TrigConfigSvc", name ),
    m_dsSvc( "TrigConf::DSConfigSvc/DSConfigSvc", name )
{

    declareProperty("BjetItem",          m_bjetItem);
    declareProperty("TrkSel_BLayer",     m_TrkSel_BLayer);
    declareProperty("TrkSel_InnerLayer",     m_TrkSel_InnerLayer);
    declareProperty("TrkSel_NextLayer",     m_TrkSel_NextLayer);
    declareProperty("TrkSel_PixHits",    m_TrkSel_PixHits);
    declareProperty("TrkSel_SiHits",     m_TrkSel_SiHits);
    declareProperty("TrkSel_D0",         m_TrkSel_D0);
    declareProperty("TrkSel_Z0",         m_TrkSel_Z0);
    declareProperty("TrkSel_Pt",         m_TrkSel_Pt);
    declareProperty("fitChi2", m_fitChi2 = 99999.);
    declareProperty("fitProb", m_fitProb = -1.);
    declareProperty("fitChi2OnNdfMax",m_fitChi2OnNdfMax=999.);
    declareProperty("DoTuning",          m_doTuning);
    declareProperty("DoTagging",         m_doTagging);

}

TrigBtagTuning::~TrigBtagTuning()
{}



////////////////////////////////////////////////////////////////////////////////////
// INITIALIZE
////////////////////////////////////////////////////////////////////////////////////
StatusCode TrigBtagTuning::initialize()
{

    ATH_MSG_INFO ("Initializing " << name() << "...");



    if (m_doTagging) {

        // OPEN INPUT FILE AND READ HISTOGRAMS
        m_infile = new TFile("tuning.root","read");

        b_input_IP1D = (TH1F*)((TH1F*)m_infile->Get("b_output_IP1D"))->Clone("b_input_IP1D"); b_input_IP1D->Scale(1.0/b_input_IP1D->GetEntries());
        b_input_IP2D = (TH1F*)((TH1F*)m_infile->Get("b_output_IP2D"))->Clone("b_input_IP2D");	b_input_IP2D->Scale(1.0/b_input_IP2D->GetEntries());
        b_input_IP3D = (TH2F*)((TH2F*)m_infile->Get("b_output_IP3D"))->Clone("b_input_IP3D");	b_input_IP3D->Scale(1.0/b_input_IP3D->GetEntries());


        b_input_IP1D_loose = (TH1F*)((TH1F*)m_infile->Get("b_output_IP1D_loose"))->Clone("b_input_IP1D_loose"); b_input_IP1D_loose->Scale(1.0/b_input_IP1D_loose->GetEntries());
        b_input_IP2D_loose = (TH1F*)((TH1F*)m_infile->Get("b_output_IP2D_loose"))->Clone("b_input_IP2D_loose");	b_input_IP2D_loose->Scale(1.0/b_input_IP2D_loose->GetEntries());
        b_input_IP3D_loose = (TH2F*)((TH2F*)m_infile->Get("b_output_IP3D_loose"))->Clone("b_input_IP3D_loose");	b_input_IP3D_loose->Scale(1.0/b_input_IP3D_loose->GetEntries());



        b_input_MVTX_Bin0 = (TH1F*)((TH1F*)m_infile->Get("b_output_MVTX"))->Clone("b_input_MVTX_Bin0");	b_input_MVTX_Bin0->Scale(1.0/b_input_MVTX_Bin0->GetEntries());
        b_input_EVTX_Bin0 = (TH1F*)((TH1F*)m_infile->Get("b_output_EVTX"))->Clone("b_input_EVTX_Bin0");	b_input_EVTX_Bin0->Scale(1.0/b_input_EVTX_Bin0->GetEntries());
        b_input_NVTX_Bin0 = (TH1F*)((TH1F*)m_infile->Get("b_output_NVTX"))->Clone("b_input_NVTX_Bin0");	b_input_NVTX_Bin0->Scale(1.0/b_input_NVTX_Bin0->GetEntries());


        b_input_MVTX = (TH1F*)((TH1F*)m_infile->Get("b_output_MVTX"))->Clone("b_input_MVTX");	

        Pb_havingSV = (b_input_MVTX->GetEntries() - b_input_MVTX->GetBinContent(1))/b_input_MVTX->GetEntries();

        b_input_MVTX->Scale(1.0/(b_input_MVTX->GetEntries() - b_input_MVTX->GetBinContent(1)));

        b_input_EVTX = (TH1F*)((TH1F*)m_infile->Get("b_output_EVTX"))->Clone("b_input_EVTX");	b_input_EVTX->Scale(1.0/(b_input_EVTX->GetEntries() - b_input_EVTX->GetBinContent(1)));
        b_input_NVTX = (TH1F*)((TH1F*)m_infile->Get("b_output_NVTX"))->Clone("b_input_NVTX");	b_input_NVTX->Scale(1.0/(b_input_NVTX->GetEntries() - b_input_NVTX->GetBinContent(1)));


        l_input_IP1D = (TH1F*)((TH1F*)m_infile->Get("l_output_IP1D"))->Clone("l_input_IP1D"); l_input_IP1D->Scale(1.0/l_input_IP1D->GetEntries());
        l_input_IP2D = (TH1F*)((TH1F*)m_infile->Get("l_output_IP2D"))->Clone("l_input_IP2D");	l_input_IP2D->Scale(1.0/l_input_IP2D->GetEntries());
        l_input_IP3D = (TH2F*)((TH2F*)m_infile->Get("l_output_IP3D"))->Clone("l_input_IP3D");	l_input_IP3D->Scale(1.0/l_input_IP3D->GetEntries());


        l_input_IP1D_loose = (TH1F*)((TH1F*)m_infile->Get("l_output_IP1D_loose"))->Clone("l_input_IP1D_loose"); l_input_IP1D_loose->Scale(1.0/l_input_IP1D_loose->GetEntries());
        l_input_IP2D_loose = (TH1F*)((TH1F*)m_infile->Get("l_output_IP2D_loose"))->Clone("l_input_IP2D_loose");	l_input_IP2D_loose->Scale(1.0/l_input_IP2D_loose->GetEntries());
        l_input_IP3D_loose = (TH2F*)((TH2F*)m_infile->Get("l_output_IP3D_loose"))->Clone("l_input_IP3D_loose");	l_input_IP3D_loose->Scale(1.0/l_input_IP3D_loose->GetEntries());


        l_input_MVTX_Bin0 = (TH1F*)((TH1F*)m_infile->Get("l_output_MVTX"))->Clone("l_input_MVTX_Bin0");	l_input_MVTX_Bin0->Scale(1.0/l_input_MVTX_Bin0->GetEntries());
        l_input_EVTX_Bin0 = (TH1F*)((TH1F*)m_infile->Get("l_output_EVTX"))->Clone("l_input_EVTX_Bin0");	l_input_EVTX_Bin0->Scale(1.0/l_input_EVTX_Bin0->GetEntries());
        l_input_NVTX_Bin0 = (TH1F*)((TH1F*)m_infile->Get("l_output_NVTX"))->Clone("l_input_NVTX_Bin0");	l_input_NVTX_Bin0->Scale(1.0/l_input_NVTX_Bin0->GetEntries());

        l_input_MVTX = (TH1F*)((TH1F*)m_infile->Get("l_output_MVTX"))->Clone("l_input_MVTX");	

        Pl_havingSV = (l_input_MVTX->GetEntries() - l_input_MVTX->GetBinContent(1))/l_input_MVTX->GetEntries();

        l_input_MVTX->Scale(1.0/(l_input_MVTX->GetEntries() - l_input_MVTX->GetBinContent(1)));

        l_input_EVTX = (TH1F*)((TH1F*)m_infile->Get("l_output_EVTX"))->Clone("l_input_EVTX");	l_input_EVTX->Scale(1.0/(l_input_EVTX->GetEntries() - l_input_EVTX->GetBinContent(1)));
        l_input_NVTX = (TH1F*)((TH1F*)m_infile->Get("l_output_NVTX"))->Clone("l_input_NVTX");	l_input_NVTX->Scale(1.0/(l_input_NVTX->GetEntries() - l_input_NVTX->GetBinContent(1)));



    }

    // OPEN OUTPUT FILE AND CREATE HISTOGRAMS

    float xbi[] = {-40.0,-20.0,-15.0,-10.0,-8.0,-6.0,-5.5,-5.0,-4.5,-4.0,-3.5,-3.0,-2.5,-2.0,-1.5,-1.0,-0.5,
        0.0,0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,8.0,10.0,15.0,20.0,40.0,60.0}; //d0
    float ybi[] = {-40.,-20.,-12.,-8.,-4.,-3.,-2.,-1.,-0.5,0.,0.5,1.,2.,3.,4.,6.,8.,12.,20.,40.,60.}; //z0



    const int nbx = sizeof(xbi)/sizeof(xbi[0]);
    const int nby = sizeof(ybi)/sizeof(ybi[0]);


    CHECK( book( TH1F("b_output_IP1D", "b_output_IP1D", nby-1,&ybi[0]) ) );
    CHECK( book( TH1F("b_output_IP2D", "b_output_IP2D", nbx-1,&xbi[0]) ) );
    CHECK( book( TH2F("b_output_IP3D", "b_output_IP3D", nby-1,&ybi[0],nbx-1,&xbi[0]) ) );
    CHECK( book( TH1F("b_output_IP1D_loose", "b_output_IP1D_loose", nby-1,&ybi[0]) ) );
    CHECK( book( TH1F("b_output_IP2D_loose", "b_output_IP2D_loose", nbx-1,&xbi[0]) ) );
    CHECK( book( TH2F("b_output_IP3D_loose", "b_output_IP3D_loose", nby-1,&ybi[0],nbx-1,&xbi[0]) ) );


    CHECK( book( TH1F("l_output_IP1D", "l_output_IP1D", nby-1,&ybi[0]) ) );
    CHECK( book( TH1F("l_output_IP2D", "l_output_IP2D", nbx-1,&xbi[0]) ) );
    CHECK( book( TH2F("l_output_IP3D", "l_output_IP3D", nby-1,&ybi[0],nbx-1,&xbi[0]) ) );
    CHECK( book( TH1F("l_output_IP1D_loose", "l_output_IP1D_loose", nby-1,&ybi[0]) ) );
    CHECK( book( TH1F("l_output_IP2D_loose", "l_output_IP2D_loose", nbx-1,&xbi[0]) ) );
    CHECK( book( TH2F("l_output_IP3D_loose", "l_output_IP3D_loose", nby-1,&ybi[0],nbx-1,&xbi[0]) ) );


    CHECK( book( TH1F("b_output_MVTX", "b_output_MVTX", 100,0,7000) ) );
    CHECK( book( TH1F("b_output_EVTX", "b_output_EVTX", 100,0,1) ) );
    CHECK( book( TH1F("b_output_NVTX", "b_output_NVTX", 14,0,14) ) );

    CHECK( book( TH1F("l_output_MVTX", "l_output_MVTX", 100,0,7000) ) );
    CHECK( book( TH1F("l_output_EVTX", "l_output_EVTX", 100,0,1) ) );
    CHECK( book( TH1F("l_output_NVTX", "l_output_NVTX", 14,0,14) ) );

    CHECK( book( TH1F("b_tag_IP1D", "b_tag_IP1D", 280,-50,20) ) );
    CHECK( book( TH1F("b_tag_IP2D", "b_tag_IP2D", 280,-50,20) ) );
    CHECK( book( TH1F("b_tag_IP3D", "b_tag_IP3D", 280,-50,20) ) );

    CHECK( book( TH1F("bEFBjet_IP1D", "bEFBjet_IP1D", 280,-50,20) ) );
    CHECK( book( TH1F("bEFBjet_IP2D", "bEFBjet_IP2D", 280,-50,20) ) );
    CHECK( book( TH1F("bEFBjet_IP3D", "bEFBjet_IP3D", 280,-50,20) ) );
    CHECK( book( TH1F("bEFBjet_SV1",  "bEFBjet_SV1",  280,-50,20) ) );

    CHECK( book( TH1F("lEFBjet_IP1D", "lEFBjet_IP1D", 280,-50,20) ) );
    CHECK( book( TH1F("lEFBjet_IP2D", "lEFBjet_IP2D", 280,-50,20) ) );
    CHECK( book( TH1F("lEFBjet_IP3D", "lEFBjet_IP3D", 280,-50,20) ) );
    CHECK( book( TH1F("lEFBjet_SV1",  "lEFBjet_SV1",  280,-50,20) ) );


    CHECK( book( TH1F("l_tag_IP1D", "l_tag_IP1D", 280,-50,20) ) );
    CHECK( book( TH1F("l_tag_IP2D", "l_tag_IP2D", 280,-50,20) ) );
    CHECK( book( TH1F("l_tag_IP3D", "l_tag_IP3D", 280,-50,20) ) );

    CHECK( book( TH1F("b_tag_SV1",  "b_tag_SV1",  280,-50,20) ) );
    CHECK( book( TH1F("b_tag_COMB", "b_tag_COMB", 280,-50,20) ) );

    CHECK( book( TH1F("l_tag_SV1",  "l_tag_SV1",  280,-50,20) ) );
    CHECK( book( TH1F("l_tag_COMB", "l_tag_COMB", 280,-50,20) ) );

    CHECK( book( TH1F("hl_offIP3D", "hl_offIP3D", 200,-50,50) ) );
    CHECK( book( TH1F("hb_offIP3D", "hb_offIP3D", 200,-50,50) ) );
    CHECK( book( TH1F("hl_offSV1",  "hl_offSV1",  200,-50,50) ) );
    CHECK( book( TH1F("hb_offSV1",  "hb_offSV1",  200,-50,50) ) );
    CHECK( book( TH1F("hl_offCOMB", "hl_offCOMB", 200,-50,50) ) );
    CHECK( book( TH1F("hb_offCOMB", "hb_offCOMB", 200,-50,50) ) );


    CHECK( book( TH1F("h_JetLabel_new","h_JetLabel_new",60,-30,30)));

    CHECK( book( TH1F("h_controlCuts",   "cut flow",       10,0,10)));
    CHECK( book( TH1F("h_controlTrkCuts","cut flow tracks",12,0,12)));

    CHECK( book( TH1F("h_pt_onlTrk","pT trk onl",100,0,300)));
    CHECK( book( TH1F("h_pt_onlJet","pT jet onl",100,0,500)));

    CHECK( book( TH1F("b_MV2c20", "b_MV2c20", 400,-1.1,1.1) ) );
    CHECK( book( TH1F("l_MV2c20", "l_MV2c20", 400,-1.1,1.1) ) );

    hist("h_controlCuts")->GetXaxis()->SetBinLabel(1,"No cut");
    hist("h_controlCuts")->GetXaxis()->SetBinLabel(2,"Online pT and Eta cut");
    hist("h_controlCuts")->GetXaxis()->SetBinLabel(3,"truth-onl match cut");
    hist("h_controlCuts")->GetXaxis()->SetBinLabel(4,"isolation cut");
    hist("h_controlCuts")->GetXaxis()->SetBinLabel(5,"offl-onl match cut");
    hist("h_controlCuts")->GetXaxis()->SetBinLabel(6,"Offline pT and Eta cut");
    hist("h_controlCuts")->GetXaxis()->SetBinLabel(7,"no jet-trks match");

    hist("h_controlTrkCuts")->GetXaxis()->SetBinLabel(1,"no cut");
    hist("h_controlTrkCuts")->GetXaxis()->SetBinLabel(2,"#DeltaR(track, jet) < 0.2");
    hist("h_controlTrkCuts")->GetXaxis()->SetBinLabel(3,"Chi2");
    hist("h_controlTrkCuts")->GetXaxis()->SetBinLabel(4,"Inner or Next layer");
    hist("h_controlTrkCuts")->GetXaxis()->SetBinLabel(5,"PixHits");
    hist("h_controlTrkCuts")->GetXaxis()->SetBinLabel(6,"SiHits");
    hist("h_controlTrkCuts")->GetXaxis()->SetBinLabel(7,"d0");
    hist("h_controlTrkCuts")->GetXaxis()->SetBinLabel(8,"z0");
    hist("h_controlTrkCuts")->GetXaxis()->SetBinLabel(9,"pt");


  
	// SERVICES
	// Retrieve StoreGate service
	StatusCode sc = m_storeGate.retrieve();
	if ( sc.isFailure() ) {
		msg(MSG::ERROR) << "Could not retrieve StoreGateSvc!" << endreq;
		return sc;
	}

	// Retrieve the trigger configuration service
	sc = m_configSvc.retrieve();
	if ( sc.isFailure() ) {
		msg(MSG::ERROR) << "Could not retrieve Trigger configuration!" << endreq;
		return sc;
	}

	// Retrieve TrigDecisionTool
	sc = m_trigDec.retrieve();
	if ( sc.isFailure() ) {
		msg(MSG::ERROR) << "Could not retrieve TrigDecisionTool!" << endreq;
		return sc;
	}

	return StatusCode::SUCCESS;
}



////////////////////////////////////////////////////////////////////////////////////
// FINALIZE
////////////////////////////////////////////////////////////////////////////////////
StatusCode TrigBtagTuning::finalize()
{

	ATH_MSG_INFO ("Finalizing " << name() << "...");

	// CLOSE AND SAVE FILES
	if (m_doTagging)
	  m_infile->Close();

//	m_outfile->Write();
//	m_outfile->Close();

	return StatusCode::SUCCESS;
}



////////////////////////////////////////////////////////////////////////////////////
// EXECUTE
////////////////////////////////////////////////////////////////////////////////////
StatusCode TrigBtagTuning::execute()
{ 

    ATH_MSG_DEBUG ("Executing " << name() << "...");


    // Access containers
    std::vector<const xAOD::Vertex*> pvContainers;
    std::vector<const xAOD::Jet*> jetContainers;
    std::vector<const xAOD::TrackParticleContainer*> tpContainers;
    std::vector<const xAOD::BTagging*> btagContainers;

    // Get features and combinations
    Trig::FeatureContainer features = m_trigDec->features(m_bjetItem);
    const std::vector< Trig::Combination >& combinations = features.getCombinations();

    // Loop on combinations
    unsigned int nCombinations = combinations.size();
    for (unsigned int combination=0; combination<nCombinations; combination++) {

        // Create data structures
        const xAOD::VertexContainer* primaryVertexContainer;
        const xAOD::JetContainer* jetContainer;
        const xAOD::TrackParticleContainer* trackParticleContainer;
        const xAOD::BTaggingContainer* btaggingContainer;

        // Get requested combination
        const Trig::Combination& comb = combinations[combination];

        // Get primary vertex container
        primaryVertexContainer = 0;
        // For split chains:
        //const std::vector< Trig::Feature<xAOD::VertexContainer> > primaryVertexContainerFeatures = comb.get<xAOD::VertexContainer>("xPrimVx");
        // For non-split chains:    
        const std::vector< Trig::Feature<xAOD::VertexContainer> > primaryVertexContainerFeatures = comb.get<xAOD::VertexContainer>("EFHistoPrmVtx");
        if (primaryVertexContainerFeatures.size()) {
            primaryVertexContainer = primaryVertexContainerFeatures[0].cptr();
            if (primaryVertexContainer == 0) {
                msg(MSG::ERROR) << "Retrieved invalid PV Input Container!" << endreq;
                return StatusCode::FAILURE;
            }   
        }   

        // Get jet container
        jetContainer = 0;
        // For split chains:   
        //const std::vector< Trig::Feature<xAOD::JetContainer> > jetContainerFeatures = comb.get<xAOD::JetContainer>("SplitJet");
        // For non-split chains:   
        const std::vector< Trig::Feature<xAOD::JetContainer> > jetContainerFeatures = comb.get<xAOD::JetContainer>();
        if (jetContainerFeatures.size()) {
            jetContainer = jetContainerFeatures[0].cptr();
        }   
        if (jetContainer == 0) {
            msg(MSG::ERROR) << "Retrieved invalid Jet Input Container!" << endreq;
            return StatusCode::FAILURE;
        } 

        // Get track particle container
        trackParticleContainer = 0;
        // For split chains:    
        //const std::vector< Trig::Feature<xAOD::TrackParticleContainer> > trackParticleContainerFeatures = comb.get<xAOD::TrackParticleContainer>("InDetTrigTrackingxAODCnv_Bjet_IDTrig");
        // For non split chains:
        const std::vector< Trig::Feature<xAOD::TrackParticleContainer> > trackParticleContainerFeatures = comb.get<xAOD::TrackParticleContainer>();
        if (trackParticleContainerFeatures.size()) {
            trackParticleContainer = trackParticleContainerFeatures[0].cptr();
        }
        if (trackParticleContainer == 0) {
            msg(MSG::ERROR) << "Retrieved invalid TP Input Container!" << endreq;
            return StatusCode::FAILURE;
        }

        // Get btagging container
        btaggingContainer = 0;
        const std::vector< Trig::Feature<xAOD::BTaggingContainer> > btaggingContainerFeatures = comb.get<xAOD::BTaggingContainer>();
        if (btaggingContainerFeatures.size()) {
            btaggingContainer = btaggingContainerFeatures[0].cptr();
        }
        if (btaggingContainer == 0) {
            msg(MSG::ERROR) << "Retrieved invalid BTag Input Container!" << endreq;
            return StatusCode::FAILURE;
        } 

        // Save data in vectors
        pvContainers.push_back((*primaryVertexContainer)[0]);
        jetContainers.push_back((*jetContainer)[0]);
        tpContainers.push_back(trackParticleContainer);
        btagContainers.push_back((*btaggingContainer)[0]);

    } 

    // Get truth jets from xAOD
    const xAOD::JetContainer* truthjets = 0;
    ATH_CHECK( evtStore()->retrieve(truthjets,"AntiKt4TruthJets") );


    //Get offline jets from xAOD
    std::string offline_container_name = "AntiKt4LCTopoJets";
    const xAOD::JetContainer* offlinejets = 0;
    ATH_CHECK(evtStore()->retrieve( offlinejets, offline_container_name));

    // selected online tracks
    xAOD::TrackParticleContainer* OnlineTracks = new xAOD::TrackParticleContainer;
    xAOD::TrackParticleAuxContainer* OnlineTracksAux = new xAOD::TrackParticleAuxContainer;

    CHECK( evtStore()->record(OnlineTracks,"OnlineTracks") );
    CHECK( evtStore()->record(OnlineTracksAux,"OnlineTracksAux.") );

    //Creates an sub-container of TrackParticles
    OnlineTracks->setStore( OnlineTracksAux );
 
   
	// Label the online bjets based on truth jets
	std::vector<int> onlinejets_label;
    std::vector<int> truth_indices;
	bjetLabel(jetContainers, truthjets, 0.3, 20.0, onlinejets_label, truth_indices);

	// Find online bjets based on proximity to other jets
	std::vector<bool> onlinejets_isolated;
	bjetIsolation(jetContainers, 0.4, onlinejets_isolated);

	// Match online bjets to online tracks to find the best matching
	std::vector<int> onlinejets_trks;
	bjetTracks(jetContainers, tpContainers, 0.4, onlinejets_trks);

	//Matching online-offline jet
	std::vector<bool> onlinejets_offline_matching;
	std::vector<int> offl_indices;
	bjetMatchOffline(jetContainers, 0.05,onlinejets_offline_matching,offl_indices);


	// Loop on online bjets
    for(unsigned int jet=0; jet<jetContainers.size(); jet++) {


        double Pt = (*(jetContainers[jet])).pt()*0.001;
        double Eta = (*(jetContainers[jet])).eta();
        double Phi_onl = (*(jetContainers[jet])).phi();
        double E_onl = (*(jetContainers[jet])).e()*0.001;

        TLorentzVector online_jetV;
        online_jetV.SetPtEtaPhiE(Pt,Eta,Phi_onl,E_onl);

        hist("h_pt_onlJet")->Fill(Pt);

        control_cuts(0);

        if ((Pt <20) || (fabs(Eta)>2.5)) continue;

        control_cuts(1);

        if (onlinejets_label[jet] == -1) continue;

        control_cuts(2);

        // Consider only fairly isolated jets
        if(!onlinejets_isolated[jet]) continue;

        control_cuts(3);

        // skip jets which don't have any offline dr<0.05
        if (!onlinejets_offline_matching[jet]) continue;

        control_cuts(4);


        /////////////////////////////////////////////////////////////////////////

        // Get the offline jet that matches the online one and store it in a TLorentzVector

        const xAOD::Jet* offlinejet = (*offlinejets)[offl_indices[jet]];

        const xAOD::Jet* truthjet = (*truthjets)[truth_indices[jet]];

        TLorentzVector offlinejetV;
        double offlinejet_Pt = offlinejet->pt()*0.001;
        double offlinejet_Eta = offlinejet->eta();
        double offlinejet_Phi = offlinejet->phi();
        double offlinejet_E = offlinejet->e()*0.001;
        offlinejetV.SetPtEtaPhiE(offlinejet_Pt,offlinejet_Eta,offlinejet_Phi,offlinejet_E);

        TLorentzVector truthjetV;
        double truthjet_Pt = truthjet->pt()*0.001;
        double truthjet_Eta = truthjet->eta();
        double truthjet_Phi = truthjet->phi();
        double truthjet_E = truthjet->e()*0.001;
        truthjetV.SetPtEtaPhiE(truthjet_Pt,truthjet_Eta,truthjet_Phi,truthjet_E);

        if ((offlinejet_Pt <20) || (fabs(offlinejet_Eta)>2.5)) continue;

        control_cuts(5);

        double dr_onl_offl = online_jetV.DeltaR(offlinejetV);
        double dr_onl_truth = online_jetV.DeltaR(truthjetV);

        int label_new=-1;

        offlinejet->getAttribute("HadronConeExclTruthLabelID",label_new);

        // clean tracks copy
        OnlineTracks->clear();
        //OfflineTracks->clear();

        //select online-offline track 
        track_index = onlinejets_trks[jet];

        if (track_index >= 0) {
    
            // Skip tracks that not match an offline jet    
            for(auto Otrack=tpContainers[track_index]->begin(); Otrack != tpContainers[track_index]->end(); Otrack++) {

                double Pt = (*Otrack)->pt()*0.001;
                double Eta = (*Otrack)->eta();
                double Phi = (*Otrack)->phi();
                double E = (*Otrack)->e()*0.001;

                hist("h_pt_onlTrk")->Fill(Pt);

                TLorentzVector onl_trk;
                onl_trk.SetPtEtaPhiE(Pt,Eta,Phi,E);

                double dr = offlinejetV.DeltaR(onl_trk);
                double dphi = offlinejetV.DeltaPhi(onl_trk);
                double deta = offlinejetV.Eta() - Eta;

                // control_Trkcuts(0);

                //cutting in the online too
                if (dr>0.2) continue; //deta not well determined in the ROI
                if (fabs(dphi)>0.2) continue;
                if (fabs(deta)>0.2) continue;

                //control_Trkcuts(1);

                xAOD::TrackParticle* newTrack_onl = new xAOD::TrackParticle; //create a new jet object

                OnlineTracks->push_back(newTrack_onl);

                *newTrack_onl = **Otrack;


            } //loop online trks

        }

        bool nontracks = false;

        // Skip jets with no track match for the tuning
        if(!m_doTagging && OnlineTracks->size()==0) { 
            nontracks = true;
            continue;
        }

        // Add weight value -46 and skip the jet in case not track match (for the tagging)
        if (m_doTagging && OnlineTracks->size()==0) nontracks = true;

        if (!nontracks) control_cuts(5);

  	// Training 
	if(m_doTuning) tuneJet(jetContainers[jet], btagContainers[jet], OnlineTracks, pvContainers[0], onlinejets_label[jet]);


  	// Validation
	if(m_doTagging) tagJet(jetContainers[jet], btagContainers[jet], (*offlinejets)[offl_indices[jet]], OnlineTracks, pvContainers[0], onlinejets_label[jet], nontracks);


  } // end loop online jets


	return StatusCode::SUCCESS;
}





/////////////////////////////////////////////////////////////////// 
// DATA ACCESS METHODS
///////////////////////////////////////////////////////////////////



void TrigBtagTuning::control_cuts (int i) {

	switch (i) {
		case 0:
		//	ATH_MSG_INFO("passing cut 0");
			hist("h_controlCuts")->Fill(0);
			break;
		case 1:
		//	ATH_MSG_INFO("passing cut 1");
			hist("h_controlCuts")->Fill(1);
			break;
		case 2: 
		//	ATH_MSG_INFO("passing cut 2");
			hist("h_controlCuts")->Fill(2);
			break;
		case 3: 
		//	ATH_MSG_INFO("passing cut 3");
			hist("h_controlCuts")->Fill(3);
			break;
		case 4: 
		//	ATH_MSG_INFO("passing cut 4");
			hist("h_controlCuts")->Fill(4);
			break;
		case 5: 
		//	ATH_MSG_INFO("passing cut 5");
			hist("h_controlCuts")->Fill(5);
			break;

	        case 6: 
		//	ATH_MSG_INFO("passing cut 5");
			hist("h_controlCuts")->Fill(6);
			break;
	}
}

void TrigBtagTuning::control_Trkcuts (int i) {

	switch (i) {
		case 0:
		//	ATH_MSG_INFO("passing Trkcut 0");
			hist("h_controlTrkCuts")->Fill(0);
			break;
		case 1:
		//	ATH_MSG_INFO("passing Trkcut 1");
		    hist("h_controlTrkCuts")->Fill(1);
			break;
		case 2: 
		//	ATH_MSG_INFO("passing Trkcut 2");
			hist("h_controlTrkCuts")->Fill(2);
			break;
		case 3: 
		//	ATH_MSG_INFO("passing Trkcut 3");
			hist("h_controlTrkCuts")->Fill(3);
			break;
		case 4: 
		//	ATH_MSG_INFO("passing Trkcut 4");
			hist("h_controlTrkCuts")->Fill(4);
			break;
		case 5:
		//	ATH_MSG_INFO("passing Trkcut 5");
			hist("h_controlTrkCuts")->Fill(5);
			break;
		case 6: 
		//	ATH_MSG_INFO("passing Trkcut 6");
			hist("h_controlTrkCuts")->Fill(6);
			break;
		case 7:
		//	ATH_MSG_INFO("passing Trkcut 7");
			hist("h_controlTrkCuts")->Fill(7);
			break;
 	        case 8:
		//	ATH_MSG_INFO("passing Trkcut 8");
			hist("h_controlTrkCuts")->Fill(8);
			break;
        	case 9:
		//	ATH_MSG_INFO("passing Trkcut 9");
			hist("h_controlTrkCuts")->Fill(9);
			break;
        	case 10:
		//	ATH_MSG_INFO("passing Trkcut 10");
			hist("h_controlTrkCuts")->Fill(10);
			break;

	}
}


void TrigBtagTuning::bjetLabel(std::vector<const xAOD::Jet*> jetContainers, const xAOD::JetContainer* truthjets, double maxDR, double minPt, std::vector<int>& onlinejets_label,std::vector<int>& truth_indices) {

	// Loop on online jets
	for(unsigned int jet=0; jet<jetContainers.size(); jet++) {

		// Label
		double minDR=9000;
		int minDR_index=-1;

		// Get TLorentzVector for online bjet
		TLorentzVector onlinejetV;
		double onlinejet_Pt = (*(jetContainers[jet])).pt()*0.001;
		double onlinejet_Eta = (*(jetContainers[jet])).eta();
		double onlinejet_Phi = (*(jetContainers[jet])).phi();
		double onlinejet_E = (*(jetContainers[jet])).e()*0.001;
		onlinejetV.SetPtEtaPhiE(onlinejet_Pt,onlinejet_Eta,onlinejet_Phi,onlinejet_E);

		// Loop on truth jets
		int index=0;
		for(auto truthjet=truthjets->begin(); truthjet != truthjets->end(); truthjet++, index++) {

			TLorentzVector truthjetV;
			double truthjet_Pt = (*truthjet)->pt()*0.001;
			double truthjet_Eta = (*truthjet)->eta();
			double truthjet_Phi = (*truthjet)->phi();
			double truthjet_E = (*truthjet)->e()*0.001;
			truthjetV.SetPtEtaPhiE(truthjet_Pt,truthjet_Eta,truthjet_Phi,truthjet_E);


			//h_pt_truth->Fill(truthjet_Pt);

			// Skip too soft truth jets
			if(truthjet_Pt<minPt) continue;

			// Update minimum DR
			double DR=onlinejetV.DeltaR(truthjetV);


			if(DR<minDR) {
				minDR = DR;
				minDR_index = index;
                        }

		}

		//h_minDR->Fill(minDR);

		// Find label for best matching truth jet
		int label_new=-1;	

		if(minDR<maxDR) {

			(*truthjets)[minDR_index]->getAttribute("HadronConeExclTruthLabelID",label_new);
			//std::cout<<"my jet label: "<<label_new<<std::endl;
		}

		// Save label
		// h_JetLabel->Fill(label_new);
        	truth_indices.push_back(minDR_index);
		onlinejets_label.push_back(label_new);
	}

}

void TrigBtagTuning::bjetMatchOffline(std::vector<const xAOD::Jet*> jetContainers, double minDR, std::vector<bool>& onlinejets_offline_matching, std::vector<int>& offl_indices) {

	// Offline 

	std::string offline_container_name = "AntiKt4LCTopoJets";
	const xAOD::JetContainer* offlinejets = 0;
	evtStore()->retrieve( offlinejets, offline_container_name);

	// Loop on online jets
	for(unsigned int jet1=0; jet1<jetContainers.size(); jet1++) {


		// Get TLorentzVector for online bjet
		TLorentzVector Onlinejet;
		double onlinejet1_Pt = (*(jetContainers[jet1])).pt()*0.001;
		double onlinejet1_Eta = (*(jetContainers[jet1])).eta();
		double onlinejet1_Phi = (*(jetContainers[jet1])).phi();
		double onlinejet1_E = (*(jetContainers[jet1])).e()*0.001;
		Onlinejet.SetPtEtaPhiE(onlinejet1_Pt,onlinejet1_Eta,onlinejet1_Phi,onlinejet1_E);

		double drmin = 999; 

		int count = 0;
		int index = -1;

		for(auto offjet_itr=offlinejets->begin() ; offjet_itr != offlinejets->end(); ++offjet_itr, ++count) {

			double pt = (*offjet_itr)->pt()*0.001;
			double eta = (*offjet_itr)->eta();
			double phi = (*offjet_itr)->phi();
			double e = (*offjet_itr)->e()*0.001;

			TLorentzVector lorentz;
			lorentz.SetPtEtaPhiE(pt,eta,phi,e);

			double dr = Onlinejet.DeltaR(lorentz);


			if (dr<drmin) {
				drmin=dr;
				index = count;

			}

		}


		//h_minDR_onl_off->Fill(drmin);

		if (drmin<minDR) {
			onlinejets_offline_matching.push_back(true);
			offl_indices.push_back(index);
        		 //   ATH_MSG_INFO("pass index true: "<<index);

		}
		else {
			onlinejets_offline_matching.push_back(false);
			offl_indices.push_back(index);
           		// ATH_MSG_INFO("pass index false: "<<index);

		}
	}

}

void TrigBtagTuning::bjetIsolation(std::vector<const xAOD::Jet*> jetContainers, double maxDR, std::vector<bool>& onlinejets_isolated) {

	// Loop on online jets
	for(unsigned int jet1=0; jet1<jetContainers.size(); jet1++) {

		// Isolation
		double minDR=9000;

		// Get TLorentzVector for online bjet
		TLorentzVector onlinejetV1;
		double onlinejet1_Pt = (*(jetContainers[jet1])).pt()*0.001;
		double onlinejet1_Eta = (*(jetContainers[jet1])).eta();
		double onlinejet1_Phi = (*(jetContainers[jet1])).phi();
		double onlinejet1_E = (*(jetContainers[jet1])).e()*0.001;
		onlinejetV1.SetPtEtaPhiE(onlinejet1_Pt,onlinejet1_Eta,onlinejet1_Phi,onlinejet1_E);

		// Loop on online jets
		for(unsigned int jet2=0; jet2<jetContainers.size(); jet2++) {

			// Skip same jet
			if(jet1==jet2) continue;

			// Get TLorentzVector for online bjet
			TLorentzVector onlinejetV2;
			double onlinejet2_Pt = (*(jetContainers[jet2])).pt()*0.001;
			double onlinejet2_Eta = (*(jetContainers[jet2])).eta();
			double onlinejet2_Phi = (*(jetContainers[jet2])).phi();
			double onlinejet2_E = (*(jetContainers[jet2])).e()*0.001;
			onlinejetV2.SetPtEtaPhiE(onlinejet2_Pt,onlinejet2_Eta,onlinejet2_Phi,onlinejet2_E);

			// Update minimum DR
			double DR=onlinejetV1.DeltaR(onlinejetV2);
			if(DR<minDR) {
				minDR = DR;
			}

		}

		// Save isolation info
		if(minDR<maxDR) onlinejets_isolated.push_back(false);
		else onlinejets_isolated.push_back(true);

	}

}



void TrigBtagTuning::bjetTracks(std::vector<const xAOD::Jet*> jetContainers, std::vector<const xAOD::TrackParticleContainer*> tpContainers, double maxDR,std::vector<int>& onlinejets_trks) {

	// Loop on online jets
	for(unsigned int jet=0; jet<jetContainers.size(); jet++) {

		// Label
		double minDR=9000;
		int minDR_index=-1;

		// Get TLorentzVector for online bjet
		TLorentzVector onlinejetV;
		double onlinejet_Pt = (*(jetContainers[jet])).pt()*0.001;
		double onlinejet_Eta = (*(jetContainers[jet])).eta();
		double onlinejet_Phi = (*(jetContainers[jet])).phi();
		double onlinejet_E = (*(jetContainers[jet])).e()*0.001;
		onlinejetV.SetPtEtaPhiE(onlinejet_Pt,onlinejet_Eta,onlinejet_Phi,onlinejet_E);

		// Loop on track vectors
		int index=0;
		for(unsigned int trk=0; trk<tpContainers.size(); trk++, index++) {

			// Update minimum DR
			double DR=Calculate_Mean(onlinejetV, tpContainers[trk]);
			//h_DR_trk_jet->Fill(DR);

			if(DR<maxDR && DR<minDR) {
				minDR = DR;
				minDR_index = index;
			}

		}

		//	ATH_MSG_INFO("minDRindex_trk_jet: "<<minDR_index);
		//	h_index_trk_jet->Fill(minDR_index);
		
		// Save track match
		onlinejets_trks.push_back(minDR_index);

	}

}




/////////////////////////////////////////////////////////////////// 
// TRACK SELECTION METHODS
///////////////////////////////////////////////////////////////////

bool TrigBtagTuning::selectTrack(const xAOD::TrackParticle* onlinetrk, float zBS, float zv, bool &type) {

/*
        std::string vertex_container_name = "PrimaryVertices";
	const xAOD::VertexContainer* vertex_sel = 0;
	evtStore()->retrieve( vertex_sel, vertex_container_name);
	xAOD::VertexContainer::const_iterator vtx_itr = vertex_sel->begin();



	const xAOD::Vertex* m_priVtx = *vtx_itr;


        double d0wrtPriVtx(0.);
		double z0wrtPriVtx(0.);

		// use new Tool for "unbiased" IP estimation //
		const Trk::ImpactParametersAndSigma* myIPandSigma(0);
		if (m_trackToVertexIPEstimator) {
			myIPandSigma = m_trackToVertexIPEstimator->estimate(onlinetrk, m_priVtx, false);
		}
		if(0==myIPandSigma) {
			ATH_MSG_WARNING("#BTAG# IPTAG: trackToVertexIPEstimator failed !");
		} else {
			d0wrtPriVtx=myIPandSigma->IPd0;
			z0wrtPriVtx=myIPandSigma->IPz0SinTheta;
			delete myIPandSigma;
			myIPandSigma=0;
		}

*/
	float pt = onlinetrk->pt()*0.001;	
	float   z0 = onlinetrk->z0() + zBS - zv;
	float   d0 = onlinetrk->d0();

//	h_pt_trk->Fill(pt);


	uint8_t nBlay=0;
	if( onlinetrk->summaryValue(nBlay,xAOD::numberOfBLayerHits) ){
	//	    ATH_MSG_INFO("Successfully retrieved the integer value, numberOfBLayerHits"); 
	}

    	uint8_t nInnerBlayer=0;
	if( onlinetrk->summaryValue(nInnerBlayer,xAOD::numberOfInnermostPixelLayerHits) ){
	//	    ATH_MSG_INFO("Successfully retrieved the integer value, numberOfInnermostPixelLayerHits"); 
	}


        uint8_t nNextBlayer=0;
	if( onlinetrk->summaryValue(nNextBlayer,xAOD::numberOfNextToInnermostPixelLayerHits) ){
	//	   ATH_MSG_INFO("Successfully retrieved the integer value, numberOfNextToInnermostPixelLayerHits"); 
	}


	uint8_t nSi=0; 
	if( onlinetrk->summaryValue(nSi,xAOD::numberOfSCTHits) ){
	//   ATH_MSG_INFO("Successfully retrieved the integer value, numberOfSCTHits"); 
	}

	uint8_t nPix=0; 
	if( onlinetrk->summaryValue(nPix,xAOD::numberOfPixelHits) ){
	// ATH_MSG_INFO("Successfully retrieved the integer value, numberOfPixelHits"); 
	}


        //ATH_MSG_INFO(" BLayerHits : "<<(int)nBlay<<" nInnerLayer: "<<(int)nInnerBlayer<<" nNextLayer: "<<(int)nNextBlayer);

	float theta = onlinetrk->theta();

        control_Trkcuts(0);

        double chi2 =  onlinetrk->chiSquared();
        int ndf = onlinetrk->numberDoF();
        double proba = 1.;
      
        if(ndf>0 && chi2>=0.) {
         Genfun::CumulativeChiSquare myCumulativeChiSquare(ndf);
         proba = 1.-myCumulativeChiSquare(chi2);
        }
        
        if(chi2>m_fitChi2) {
         //		ATH_MSG_INFO("doesn't pass fitChi2 cut: "<<chi2);
	 return false;
        }

     if(proba<m_fitProb) {		
      //  ATH_MSG_INFO("doesn't pass fitProb cut: "<<proba);
      return false;
     }

     if(ndf>0) {
       if(chi2/double(ndf)>m_fitChi2OnNdfMax) {
       	//	ATH_MSG_INFO("doesn't pass chi2/ndf cut: "<<chi2/double(ndf));
	return false;
       }
     } else {
     	//	ATH_MSG_INFO("doesn't pass ndf cut: "<<ndf);
	return false;
       }

     control_Trkcuts(2);

  //  if (nBlay < m_TrkSel_BLayer) {
  //      return false;
  //  }
    
     //new selection
    
     if(nInnerBlayer < m_TrkSel_InnerLayer && nNextBlayer < m_TrkSel_NextLayer) {

 	//if (nBlay >= 1) ATH_MSG_INFO("BLayer: "<<(int)nBlay<<" InnerLayer: "<<(int)nInnerBlayer<<" NextLayer: "<<(int)nNextBlayer);
	
        return false;
     }

     else if (nInnerBlayer < m_TrkSel_InnerLayer) type = false;

     control_Trkcuts(3);

     if(nPix < m_TrkSel_PixHits) {
	//ATH_MSG_INFO("doesn't pass Pixel cut: "<<onlinetrk->summaryValue(nPix,xAOD::numberOfPixelHits));
	return false;
     }

     control_Trkcuts(4);

     if(nSi+nPix < m_TrkSel_SiHits) {
	//ATH_MSG_INFO("doesn't pass SCT cut: "<<nSi+nPix);
	return false;
     }

     control_Trkcuts(5);

     if(fabs(d0) > m_TrkSel_D0) {
	//ATH_MSG_INFO("doesn't pass d0 cut: "<<fabs(d0));
	return false;
     }

     control_Trkcuts(6);

     if(fabs(z0*TMath::Sin(theta)) > m_TrkSel_Z0) {
	//ATH_MSG_INFO("doesn't pass z0 cut: "<<fabs(z0*TMath::Sin(theta)));
	return false;
     }

     control_Trkcuts(7);

     if(fabs(pt)< m_TrkSel_Pt) {
	//ATH_MSG_INFO("doesn't pass Pt cut: "<<fabs(pt));
	return false;
     }

     control_Trkcuts(8);

     return true;

}



/////////////////////////////////////////////////////////////////// 
// TUNING METHODS
///////////////////////////////////////////////////////////////////


void TrigBtagTuning::tuneJet(const xAOD::Jet* jetContainers, const xAOD::BTagging* btagContainers, const xAOD::TrackParticleContainer* onlinetrks, const xAOD::Vertex* pvContainers, int label) {


    std::string vertex_container_name = "PrimaryVertices";
    const xAOD::VertexContainer* vertex = 0;
    evtStore()->retrieve( vertex, vertex_container_name);
    xAOD::VertexContainer::const_iterator vtx_itr = vertex->begin();


    // Get pv value
    float zv  = pvContainers->z();
    float zv_offl  = (*vtx_itr)->z();


    // Fill track-based PDFs
    for(auto track=onlinetrks->begin(); track != onlinetrks->end(); track++) {

        float zBS = (*track)->vz();

        bool type=true;

        // Select track
        if(!selectTrack(*track, zBS, zv, type)) continue;

        // Fill histograms
        float z0_wrt_pv = (*track)->z0() + zBS - zv;

        float d0_wrt_pv = (*track)->d0();

        float z0_sign = z0Sign(z0_wrt_pv, (*track)->eta(), (*(jetContainers)).eta());
        float d0_sign = d0Sign(d0_wrt_pv, (*track)->phi(), (*(jetContainers)).phi());


        float z0_signed = std::fabs(z0_wrt_pv)*z0_sign;
        float d0_signed = std::fabs(d0_wrt_pv)*d0_sign;


        const xAOD::ParametersCovMatrix_t covTrk = (*track)->definingParametersCovMatrix();
        float z0_significance = z0_signed/Amg::error(covTrk, 1);

        float d0_significance = d0_signed/Amg::error(covTrk, 0);

        //underflows/overflows correction
        if (d0_significance<= -40.0) d0_significance = -39.9; 
        if (z0_significance<= -40.0) z0_significance = -39.9; 
        if (d0_significance>= 60.0) d0_significance = 59.9; 
        if (z0_significance>= 60.0) z0_significance = 59.9; 


        if(label==5 && type) {
            hist("b_output_IP1D")->Fill(z0_significance);
            hist("b_output_IP2D")->Fill(d0_significance);
            hist("b_output_IP3D")->Fill(z0_significance,d0_significance);
        }
        else if (label==5) {
            hist("b_output_IP1D_loose")->Fill(z0_significance);
            hist("b_output_IP2D_loose")->Fill(d0_significance);
            hist("b_output_IP3D_loose")->Fill(z0_significance,d0_significance);
        }

        if((label==0) && type) {
            hist("l_output_IP1D")->Fill(z0_significance);
            hist("l_output_IP2D")->Fill(d0_significance);
            hist("l_output_IP3D")->Fill(z0_significance,d0_significance);
        }
        else if (label==0) {
            hist("l_output_IP1D_loose")->Fill(z0_significance);
            hist("l_output_IP2D_loose")->Fill(d0_significance);
            hist("l_output_IP3D_loose")->Fill(z0_significance,d0_significance);
        }	

    } //loop trks


    // accessing SV info:
    float mvtx(0);
    btagContainers->variable<float>("SV1", "masssvx", mvtx);
    ATH_MSG_INFO("massvx: "<<mvtx);

    float evtx(0);
    btagContainers->variable<float>("SV1", "efracsvx", evtx);
    ATH_MSG_INFO("efracsvx: "<<evtx);

    int nvtx(0);
    btagContainers->variable<int>("SV1", "N2Tpair", nvtx);
    ATH_MSG_INFO("N2Tpair: "<<nvtx);

    // accessing MV2c20 info:
    double mv2c20(0);
    btagContainers->variable<double>("MV2c20","discriminant",mv2c20);


    // Fill jet-based PDFs
    if(label==5) {

        hist("b_output_MVTX")->Fill(mvtx);
        hist("b_output_EVTX")->Fill(evtx);
        hist("b_output_NVTX")->Fill(nvtx);
        hist("b_MV2c20")->Fill(mv2c20);

    } 

    else if(label==0) {

        hist("l_output_MVTX")->Fill(mvtx);
        hist("l_output_EVTX")->Fill(evtx);
        hist("l_output_NVTX")->Fill(nvtx);
        hist("l_MV2c20")->Fill(mv2c20);

    }
}






/////////////////////////////////////////////////////////////////// 
// TAGGING METHODS
///////////////////////////////////////////////////////////////////



void TrigBtagTuning::tagJet(const xAOD::Jet* jetContainers, const xAOD::BTagging* btagContainers, const xAOD::Jet* offlinejet, const xAOD::TrackParticleContainer* onlinetrks, const xAOD::Vertex* pvContainers, int label, bool nontracks) {

    bool has_notracks = nontracks;	

    //Use online vertex
    float zv = pvContainers->z();

    //  const xAOD::Vertex* m_priVtx = *vtx_itr_xaod;

    // Calculate track-based weights
    float lr_IP1D=1.0, lr_IP2D=1.0, lr_IP3D=1.0;

    bool pass_selection=false;

    if (!has_notracks) {

        for(auto track=onlinetrks->begin(); track != onlinetrks->end(); track++) {

            float zBS = (*track)->vz();

            bool type = true;

            // Select track
            if(!selectTrack(*track, zBS, zv, type)) continue;

            pass_selection=true;

            // Get likelihood ratios
            float z0_wrt_pv = (*track)->z0() + zBS - zv;
            //	float z0_wrt_pv = z0wrtPriVtx; // -->xAOD
            float d0_wrt_pv = (*track)->d0();
            //float d0_wrt_pv = d0wrtPriVtx;

            float z0_sign = z0Sign(z0_wrt_pv, (*track)->eta(), (*(jetContainers)).eta());
            float d0_sign = d0Sign(d0_wrt_pv, (*track)->phi(), (*(jetContainers)).phi());

            float z0_signed = std::fabs(z0_wrt_pv)*z0_sign;
            float d0_signed = std::fabs(d0_wrt_pv)*d0_sign;

            const xAOD::ParametersCovMatrix_t covTrk = (*track)->definingParametersCovMatrix();
            float z0_significance = z0_signed/Amg::error(covTrk, 1);
            //float z0_significance = z0_signed/z0ErrwrtPriVtx;
            float d0_significance = d0_signed/Amg::error(covTrk, 0);
            //float d0_significance = d0_signed/d0ErrwrtPriVtx;

            //underflows/overflows correction
            if (d0_significance<= -40.0) d0_significance = -39.9; 
            if (z0_significance<= -40.0) z0_significance = -39.9; 
            if (d0_significance>= 60.0) d0_significance = 59.9; 
            if (z0_significance>= 60.0) z0_significance = 59.9; 


            if (type) {


                if(l_input_IP1D->GetBinContent(l_input_IP1D->FindBin(z0_significance))==0) lr_IP1D *=100.0;  
                else lr_IP1D *= (b_input_IP1D->GetBinContent(b_input_IP1D->FindBin(z0_significance)) / l_input_IP1D->GetBinContent(l_input_IP1D->FindBin(z0_significance)));

                if(l_input_IP2D->GetBinContent(l_input_IP2D->FindBin(d0_significance))==0) lr_IP2D *=100.0;
                else lr_IP2D *= (b_input_IP2D->GetBinContent(b_input_IP2D->FindBin(d0_significance)) / l_input_IP2D->GetBinContent(l_input_IP2D->FindBin(d0_significance)));

                if(l_input_IP3D->GetBinContent(l_input_IP3D->FindBin(z0_significance,d0_significance))==0) lr_IP3D *=100.0;
                else lr_IP3D *= (b_input_IP3D->GetBinContent(b_input_IP3D->FindBin(z0_significance,d0_significance)) / l_input_IP3D->GetBinContent(l_input_IP3D->FindBin(z0_significance,d0_significance)));

            }

            else {

                if(l_input_IP1D_loose->GetBinContent(l_input_IP1D_loose->FindBin(z0_significance))==0) lr_IP1D *=100.0;  
                else lr_IP1D *= (b_input_IP1D_loose->GetBinContent(b_input_IP1D_loose->FindBin(z0_significance)) / l_input_IP1D_loose->GetBinContent(l_input_IP1D_loose->FindBin(z0_significance)));

                if(l_input_IP2D_loose->GetBinContent(l_input_IP2D_loose->FindBin(d0_significance))==0) lr_IP2D *=100.0;
                else lr_IP2D *= (b_input_IP2D_loose->GetBinContent(b_input_IP2D_loose->FindBin(d0_significance)) / l_input_IP2D_loose->GetBinContent(l_input_IP2D_loose->FindBin(d0_significance)));

                if(l_input_IP3D_loose->GetBinContent(l_input_IP3D_loose->FindBin(z0_significance,d0_significance))==0) lr_IP3D *=100.0;
                else lr_IP3D *= (b_input_IP3D_loose->GetBinContent(b_input_IP3D_loose->FindBin(z0_significance,d0_significance)) / l_input_IP3D_loose->GetBinContent(l_input_IP3D_loose->FindBin(z0_significance,d0_significance)));

            }

        }

    }

    float w_IP1D, w_IP2D, w_IP3D;
    if (pass_selection) {
        w_IP1D=TMath::Log10(lr_IP1D); 
        w_IP2D=TMath::Log10(lr_IP2D);
        w_IP3D=TMath::Log10(lr_IP3D);
    }

    else if (has_notracks) {
        w_IP1D=-46; 
        w_IP2D=-46;
        w_IP3D=-46;
    }
    else {
        w_IP1D=-45; 
        w_IP2D=-45;
        w_IP3D=-45;

    }

    //temporary
    if (w_IP1D<=-50.0) w_IP1D=-49.0;
    if (w_IP2D<=-50.0) w_IP2D=-49.0;
    if (w_IP3D<=-50.0) w_IP3D=-49.0;


    // Calculate jet-based weights
    float lr_SV1=1.0;

    // accessing SV info:
    float mvtx(0);
    btagContainers->variable<float>("SV1", "masssvx", mvtx);
    ATH_MSG_INFO("massvx: "<<mvtx);

    float evtx(0);
    btagContainers->variable<float>("SV1", "efracsvx", evtx);
    ATH_MSG_INFO("efracsvx: "<<evtx);

    int nvtx(0);
    btagContainers->variable<int>("SV1", "N2Tpair", nvtx);
    ATH_MSG_INFO("N2Tpair: "<<nvtx);

    // accessing MV2c20 info:
    double mv2c20(0);
    btagContainers->variable<double>("MV2c20","discriminant",mv2c20);

    //input_MVTX_Bin0 is normalized with the total entries but I use only the first bin = P(no having a SV)
    //input_MVTX is normalized without considering the first bin. I use all bins but not the first one (mvtx != 0). = P(mvtx,nvtx,evtx)
    //We should still add the P(having a SV)b = (b_input_MVTX->GetEntries() - b_input_MVTX->GetBinContent(1))/b_input_MVTX->GetEntries(); 


    if (mvtx == 0) {

        lr_SV1 *= (b_input_MVTX_Bin0->GetBinContent(1) / l_input_MVTX_Bin0->GetBinContent(1));

    }

    // Adding the P(vertex) in case I have SV. 
    //Pu = 1-Pvertex(u) —> if we have no vertex
    //Pu = Pvertex(u)*Pnvtx(u)*Pmass(u)*Pfrac(u) —> if we have the vertex

    else {

        lr_SV1 *= Pb_havingSV/Pl_havingSV; //Adding P(having a SV)

        if(l_input_MVTX->GetBinContent(l_input_MVTX->FindBin(mvtx))==0)  lr_SV1 *=100.0;
        else lr_SV1 *= (b_input_MVTX->GetBinContent(b_input_MVTX->FindBin(mvtx)) / l_input_MVTX->GetBinContent(l_input_MVTX->FindBin(mvtx)));

        if(l_input_EVTX->GetBinContent(l_input_EVTX->FindBin(evtx))==0) lr_SV1 *=100.0;
        else lr_SV1 *= (b_input_EVTX->GetBinContent(b_input_EVTX->FindBin(evtx)) / l_input_EVTX->GetBinContent(l_input_EVTX->FindBin(evtx)));

        if(l_input_NVTX->GetBinContent(l_input_NVTX->FindBin(nvtx))==0) lr_SV1 *=100.0;
        else lr_SV1 *= (b_input_NVTX->GetBinContent(b_input_NVTX->FindBin(nvtx)) / l_input_NVTX->GetBinContent(l_input_NVTX->FindBin(nvtx)));

    }

    float lr_COMB;

    lr_COMB = lr_IP3D*lr_SV1;

    //float w_COMB = -1.0*TMath::Log10(1-(lr_COMB/(1+lr_COMB)));
    float w_COMB = TMath::Log10(lr_COMB);
    float w_SV1 =  TMath::Log10(lr_SV1);


    const xAOD::BTagging* btag=0;

    btag =  offlinejet->btagging();


    if (label==5) {
        hist("hb_offIP3D")->Fill(btag->IP3D_loglikelihoodratio());
        hist("hb_offSV1")->Fill(btag->SV1_loglikelihoodratio());
        hist("hb_offCOMB")->Fill(btag->SV1plusIP3D_discriminant());
        hist("b_tag_IP1D")->Fill(w_IP1D);
        hist("b_tag_IP2D")->Fill(w_IP2D);
        hist("b_tag_IP3D")->Fill(w_IP3D);
        hist("b_tag_COMB")->Fill(w_COMB);
        hist("b_tag_SV1")->Fill(w_SV1);
        hist("b_MV2c20")->Fill(mv2c20);     

    }


    if (label==0) {
        hist("hl_offIP3D")->Fill(btag->IP3D_loglikelihoodratio());
        hist("hl_offSV1")->Fill(btag->SV1_loglikelihoodratio());
        hist("hl_offCOMB")->Fill(btag->SV1plusIP3D_discriminant());
        hist("l_tag_IP1D")->Fill(w_IP1D);
        hist("l_tag_IP2D")->Fill(w_IP2D);
        hist("l_tag_IP3D")->Fill(w_IP3D);
        hist("l_tag_COMB")->Fill(w_COMB);
        hist("l_tag_SV1")->Fill(w_SV1);
        hist("l_MV2c20")->Fill(mv2c20);     

    }

}


/////////////////////////////////////////////////////////////////// 
// AUXILIARY METHODS
///////////////////////////////////////////////////////////////////


int TrigBtagTuning :: TruthMatch(TLorentzVector myjet, std::vector<TLorentzVector> jets, std::vector<TLorentzVector> &matchingJets, double Cut) {
	int Nmatches=0;
	for (uint ji=0;ji<jets.size();++ji)
		if (myjet.DeltaR(jets[ji])<Cut) {
			//if (Nmatches==0) matchingJet=jets[ji];
			if (Nmatches==0) matchingJets.clear();
			matchingJets.push_back(jets[ji]);
			++Nmatches;
		}
	return Nmatches;
}

double TrigBtagTuning :: DRmin(TLorentzVector myjet, std::vector<TLorentzVector> jets, double PtMin) {

	double DRmin=9999;
	for (uint ji=0;ji<jets.size();++ji) {
		if (PtMin>0&&jets[ji].Pt()<PtMin) continue;
		double DR=myjet.DeltaR(jets[ji]);

		if ( DR<DRmin ) DRmin=DR;            

	}
	return DRmin;
}

float  TrigBtagTuning :: d0Sign(float d0, float phi, float jetPhi) {

	float deltaPhi = jetPhi - phi;
	float sign = d0*TMath::Sin(deltaPhi);

	if (sign<0) 
		return -1.0;
	else
		return 1.0;
}

float TrigBtagTuning :: z0Sign(float z0, float eta, float jetEta) {

	float deltaEta = jetEta - eta;
	float sign = z0*deltaEta;

	if (sign<0) 
		return -1.0;
	else
		return 1.0;
}

float  TrigBtagTuning :: Calculate_Mean (TLorentzVector myjet, const xAOD::TrackParticleContainer* tpContainers) {


    float den=0, num=0;
    float mean;

    for (auto track_itr=tpContainers->begin() ; track_itr != tpContainers->end(); ++track_itr) {


        double Pt = (*track_itr)->pt()*0.001;
        double Eta = (*track_itr)->eta();
        double Phi = (*track_itr)->phi();
        double E = (*track_itr)->e()*0.001;

        TLorentzVector track;

        track.SetPtEtaPhiE(Pt,Eta,Phi,E);

        float dr = myjet.DeltaR(track);
        den += Pt;
        num += Pt*dr;
    }
    mean = num/den;

    return mean;
}
