from RecExConfig.RecFlags import rec
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags as acf
from AthenaPoolCnvSvc import ReadAthenaPool
from AthenaCommon.AppMgr import ToolSvc
import glob

rec.readAOD=True

#Skip first 47000 events
#acf.SkipEvents=47000 
#
#Run on the firsts 47000 events only
#acf.EvtMax=47000

#Run in all events
acf.EvtMax=-1


# To run in multiple files use: *


#complete EOS sample (do "eosmount eos" in ~/ before using it)
#lista = glob.glob('/afs/cern.ch/user/g/gmarceca/eos/atlas/atlasdatadisk/rucio/valid1/*/*/AOD.01575399._*.pool.root.1')

#Local file:
#lista = glob.glob('/afs/cern.ch/work/c/cschiavi/public/DATA-VALIDATION/valid1.110401.PowhegPythia_P2012_ttbar_nonallhad.recon.AOD.e3099_s2127_r6119_tid04871431_00/AOD.04871431._000059.pool.root.1')
#lista = glob.glob('/afs/cern.ch/user/g/gmarceca/WORK/sample_test/valid1.110401.PowhegPythia_P2012_ttbar_nonallhad.recon.AOD.e3099_s2127_r6119_tid04871431_00/AOD.04871431._000070.pool.root.1')
#lista = glob.glob('/afs/cern.ch/user/g/gmarceca/WORK/grid_samples/valid2.110401.PowhegPythia_P2012_ttbar_nonallhad.recon.AOD.e3099_s2578_r6538_tid05237802_00/AOD.05237802._000020.pool.root.1')
lista = glob.glob('/afs/cern.ch/user/g/gmarceca/WORK/public/valid2.110401.PowhegPythia_P2012_ttbar_nonallhad.recon.AOD.e3099_s2578_r6538/AOD.05237802._000020.pool.root.1')

acf.FilesInput=lista 


rec.doCBNT=False

from RecExConfig.RecFlags import rec
rec.doTrigger=True
from RecExConfig.RecAlgsFlags  import recAlgs
recAlgs.doTrigger=True
from TriggerJobOpts.TriggerFlags import TriggerFlags
TriggerFlags.doTriggerConfigOnly=True


rec.doWriteAOD=False
rec.doWriteESD=False
rec.doWriteTAG=False
rec.doAOD=False
rec.doDPD=False 
rec.doESD=False
rec.doCBNT = False
rec.doHist = False  #default (True)

doTAG=False

rec.doTruth=False

#import IPEstimator algorithms
#from TrkExTools.AtlasExtrapolator import AtlasExtrapolator
#AtlasExtrapolator = AtlasExtrapolator()
#ToolSvc += AtlasExtrapolator
#
#from TrkVertexFitterUtils.TrkVertexFitterUtilsConf import Trk__FullLinearizedTrackFactory
#myBTagLinTrkFactory = Trk__FullLinearizedTrackFactory(name="BTagFullLinearizedTrackFactory",
#Extrapolator= AtlasExtrapolator)
#ToolSvc += myBTagLinTrkFactory
#
#
#from TrkVertexFitterUtils.TrkVertexFitterUtilsConf import Trk__TrackToVertexIPEstimator
#BTagTrackToVertexIPEstimator = Trk__TrackToVertexIPEstimator(name="BTagTrackToVertexIPEstimator",
#Extrapolator=AtlasExtrapolator,
#LinearizedTrackFactory=myBTagLinTrkFactory)
#ToolSvc += BTagTrackToVertexIPEstimator


#-----------------------------------------------------------
include("RecExCommon/RecExCommon_topOptions.py")
#-----------------------------------------------------------

# abort when there is an unchecked status code
StatusCodeSvc.AbortOnError=False



myAlg = CfgMgr.TrigBtagTuning("TrigBtagTuning",BjetItem="HLT_j55_bperf",DoTuning=True,DoTagging=False)

##New Track Selection
myAlg.TrkSel_BLayer = 1;
myAlg.TrkSel_InnerLayer = 1;
myAlg.TrkSel_NextLayer = 1;
myAlg.TrkSel_PixHits = 2;
myAlg.TrkSel_SiHits = 7;
myAlg.TrkSel_D0 = 1; #[mm]
myAlg.TrkSel_Z0 = 1.5; #[mm]
myAlg.TrkSel_Pt = 1; #[GeV]

#myAlg.TrackToVertexIPEstimator = BTagTrackToVertexIPEstimator;

rootStreamName = "MyFirstHistoStream"
rootFileName   = "myHistosAth.root"
myAlg.RootStreamName   = rootStreamName
#myAlg.RootDirName      = "/MyHists"



from AthenaCommon.AlgSequence import AlgSequence
Sequence = AlgSequence()
Sequence += myAlg


# ====================================================================
# Define your output root file using MultipleStreamManager
# ====================================================================
from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
MyFirstHistoXAODStream = MSMgr.NewRootStream( rootStreamName, rootFileName )


